

//Add the following users:
db.users.insertMany([
	{
		"firstName": "Diane",
		"lastName": "Murphy",
		"email": "dmurphy@mail.com",
		"isAdmin": false,
		"isActive": true
	},
	{
		"firstName": "Mary",
		"lastName": "Patterson",
		"email": "mpatterson@mail.com",
		"isAdmin": false,
		"isActive": true
	},
	{
		"firstName": "Jeff",
		"lastName": "Firrelli",
		"email": "jfirrelli@mail.com",
		"isAdmin": false,
		"isActive": true
	},
	{
		"firstName": "Gerard",
		"lastName": "Bondur",
		"email": "gbondur@mail.com",
		"isAdmin": false,
		"isActive": true
	},
	{
		"firstName": "Pamela",
		"lastName": "Castillo",
		"email": "pcastillo@mail.com",
		"isAdmin": true,
		"isActive": false
	},
	{
		"firstName": "George",
		"lastName": "Vanauf",
		"email": "gvanauf@mail.com",
		"isAdmin": true,
		"isActive": true
	}
]);


//Add the following courses:

db.courses.insertMany([
	{
		"name": "Professional Development",
		"price": 10000
	},
	{
		"name": "Business Processing",
		"price": 13000
	}
]);


//Get the users who are not administrators
db.users.find({"isAdmin": false});


//Get user IDs and add them as enrollees of the courses
db.courses.updateOne(
	{
		"name": "Professional Development"
	},
	{
		$set: {
			"enrollees": [
			{"userId": ObjectId("620cc496a79c59764ae1846b")},
			{"userId": ObjectId("620cc496a79c59764ae1846c")}
			]
		}
	}
)