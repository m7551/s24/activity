
//Create a new collection of users.
db.users.insertMany([
	{
			"firstName": "Stephen",
			"lastName": "Hawking",
			"age": 76,
			"email": "stephenhawking@mail.com",
			"department": "HR"
		},
		{
			"firstName": "Neil",
			"lastName": "Armstrong",
			"age": 82,
			"email": "neilarmstrong@mail.com",
			"department": "HR"
		},
		{
			"firstName": "Bill",
			"lastName": "Gate",
			"age": 65,
			"email": "billgates@mail.com",
			"department": "Operations"
		},
		{
			"firstName": "Jane",
			"lastName": "Doe",
			"age": 21,
			"email": "janedoe@mail.com",
			"department": "HR"
		}
	])


//Find users with letter 's' in their firstName or 'd' in their lastName
/*
	a. Use $or
	b. Only show firstName and lastName
*/

db.users.find({
	$or: [{"firstName":{$regex: 's',$options:'$i'}}, {"lastName": {$regex:'d',$options:'$i'}}]
}, {"firstName": 1, "lastName": 1, "_id": 0});


//Find users who are from the HR dept. and their age is >= 70
//use $and
db.users.find({$and: [{"department": "HR"}, {"age": {$gte:70}}]})


//Find user with letter 'e' in first name and age of <= 30
// user $and, $regex, $lte
db.users.find({
	$and: [{"firstName":{$regex: 'e',$options:'$i'}}, {"age": {$lte: 30}}]
});